# CMAKE generated file: DO NOT EDIT!
# Generated by "Unix Makefiles" Generator, CMake Version 3.10

# Delete rule output on recipe failure.
.DELETE_ON_ERROR:


#=============================================================================
# Special targets provided by cmake.

# Disable implicit rules so canonical targets will work.
.SUFFIXES:


# Remove some rules from gmake that .SUFFIXES does not remove.
SUFFIXES =

.SUFFIXES: .hpux_make_needs_suffix_list


# Suppress display of executed commands.
$(VERBOSE).SILENT:


# A target that is always out of date.
cmake_force:

.PHONY : cmake_force

#=============================================================================
# Set environment variables for the build.

# The shell in which to execute make rules.
SHELL = /bin/sh

# The CMake executable.
CMAKE_COMMAND = /usr/bin/cmake

# The command to remove a file.
RM = /usr/bin/cmake -E remove -f

# Escaping for special characters.
EQUALS = =

# The top-level source directory on which CMake was run.
CMAKE_SOURCE_DIR = /home/alakt/musiclab

# The top-level build directory on which CMake was run.
CMAKE_BINARY_DIR = /home/alakt/musiclab/bin

# Include any dependencies generated for this target.
include CMakeFiles/musiclab.dir/depend.make

# Include the progress variables for this target.
include CMakeFiles/musiclab.dir/progress.make

# Include the compile flags for this target's objects.
include CMakeFiles/musiclab.dir/flags.make

CMakeFiles/musiclab.dir/src/main.cpp.o: CMakeFiles/musiclab.dir/flags.make
CMakeFiles/musiclab.dir/src/main.cpp.o: ../src/main.cpp
	@$(CMAKE_COMMAND) -E cmake_echo_color --switch=$(COLOR) --green --progress-dir=/home/alakt/musiclab/bin/CMakeFiles --progress-num=$(CMAKE_PROGRESS_1) "Building CXX object CMakeFiles/musiclab.dir/src/main.cpp.o"
	/usr/bin/c++  $(CXX_DEFINES) $(CXX_INCLUDES) $(CXX_FLAGS) -o CMakeFiles/musiclab.dir/src/main.cpp.o -c /home/alakt/musiclab/src/main.cpp

CMakeFiles/musiclab.dir/src/main.cpp.i: cmake_force
	@$(CMAKE_COMMAND) -E cmake_echo_color --switch=$(COLOR) --green "Preprocessing CXX source to CMakeFiles/musiclab.dir/src/main.cpp.i"
	/usr/bin/c++ $(CXX_DEFINES) $(CXX_INCLUDES) $(CXX_FLAGS) -E /home/alakt/musiclab/src/main.cpp > CMakeFiles/musiclab.dir/src/main.cpp.i

CMakeFiles/musiclab.dir/src/main.cpp.s: cmake_force
	@$(CMAKE_COMMAND) -E cmake_echo_color --switch=$(COLOR) --green "Compiling CXX source to assembly CMakeFiles/musiclab.dir/src/main.cpp.s"
	/usr/bin/c++ $(CXX_DEFINES) $(CXX_INCLUDES) $(CXX_FLAGS) -S /home/alakt/musiclab/src/main.cpp -o CMakeFiles/musiclab.dir/src/main.cpp.s

CMakeFiles/musiclab.dir/src/main.cpp.o.requires:

.PHONY : CMakeFiles/musiclab.dir/src/main.cpp.o.requires

CMakeFiles/musiclab.dir/src/main.cpp.o.provides: CMakeFiles/musiclab.dir/src/main.cpp.o.requires
	$(MAKE) -f CMakeFiles/musiclab.dir/build.make CMakeFiles/musiclab.dir/src/main.cpp.o.provides.build
.PHONY : CMakeFiles/musiclab.dir/src/main.cpp.o.provides

CMakeFiles/musiclab.dir/src/main.cpp.o.provides.build: CMakeFiles/musiclab.dir/src/main.cpp.o


CMakeFiles/musiclab.dir/src/note/TimedNote.cpp.o: CMakeFiles/musiclab.dir/flags.make
CMakeFiles/musiclab.dir/src/note/TimedNote.cpp.o: ../src/note/TimedNote.cpp
	@$(CMAKE_COMMAND) -E cmake_echo_color --switch=$(COLOR) --green --progress-dir=/home/alakt/musiclab/bin/CMakeFiles --progress-num=$(CMAKE_PROGRESS_2) "Building CXX object CMakeFiles/musiclab.dir/src/note/TimedNote.cpp.o"
	/usr/bin/c++  $(CXX_DEFINES) $(CXX_INCLUDES) $(CXX_FLAGS) -o CMakeFiles/musiclab.dir/src/note/TimedNote.cpp.o -c /home/alakt/musiclab/src/note/TimedNote.cpp

CMakeFiles/musiclab.dir/src/note/TimedNote.cpp.i: cmake_force
	@$(CMAKE_COMMAND) -E cmake_echo_color --switch=$(COLOR) --green "Preprocessing CXX source to CMakeFiles/musiclab.dir/src/note/TimedNote.cpp.i"
	/usr/bin/c++ $(CXX_DEFINES) $(CXX_INCLUDES) $(CXX_FLAGS) -E /home/alakt/musiclab/src/note/TimedNote.cpp > CMakeFiles/musiclab.dir/src/note/TimedNote.cpp.i

CMakeFiles/musiclab.dir/src/note/TimedNote.cpp.s: cmake_force
	@$(CMAKE_COMMAND) -E cmake_echo_color --switch=$(COLOR) --green "Compiling CXX source to assembly CMakeFiles/musiclab.dir/src/note/TimedNote.cpp.s"
	/usr/bin/c++ $(CXX_DEFINES) $(CXX_INCLUDES) $(CXX_FLAGS) -S /home/alakt/musiclab/src/note/TimedNote.cpp -o CMakeFiles/musiclab.dir/src/note/TimedNote.cpp.s

CMakeFiles/musiclab.dir/src/note/TimedNote.cpp.o.requires:

.PHONY : CMakeFiles/musiclab.dir/src/note/TimedNote.cpp.o.requires

CMakeFiles/musiclab.dir/src/note/TimedNote.cpp.o.provides: CMakeFiles/musiclab.dir/src/note/TimedNote.cpp.o.requires
	$(MAKE) -f CMakeFiles/musiclab.dir/build.make CMakeFiles/musiclab.dir/src/note/TimedNote.cpp.o.provides.build
.PHONY : CMakeFiles/musiclab.dir/src/note/TimedNote.cpp.o.provides

CMakeFiles/musiclab.dir/src/note/TimedNote.cpp.o.provides.build: CMakeFiles/musiclab.dir/src/note/TimedNote.cpp.o


# Object files for target musiclab
musiclab_OBJECTS = \
"CMakeFiles/musiclab.dir/src/main.cpp.o" \
"CMakeFiles/musiclab.dir/src/note/TimedNote.cpp.o"

# External object files for target musiclab
musiclab_EXTERNAL_OBJECTS =

musiclab: CMakeFiles/musiclab.dir/src/main.cpp.o
musiclab: CMakeFiles/musiclab.dir/src/note/TimedNote.cpp.o
musiclab: CMakeFiles/musiclab.dir/build.make
musiclab: CMakeFiles/musiclab.dir/link.txt
	@$(CMAKE_COMMAND) -E cmake_echo_color --switch=$(COLOR) --green --bold --progress-dir=/home/alakt/musiclab/bin/CMakeFiles --progress-num=$(CMAKE_PROGRESS_3) "Linking CXX executable musiclab"
	$(CMAKE_COMMAND) -E cmake_link_script CMakeFiles/musiclab.dir/link.txt --verbose=$(VERBOSE)

# Rule to build all files generated by this target.
CMakeFiles/musiclab.dir/build: musiclab

.PHONY : CMakeFiles/musiclab.dir/build

CMakeFiles/musiclab.dir/requires: CMakeFiles/musiclab.dir/src/main.cpp.o.requires
CMakeFiles/musiclab.dir/requires: CMakeFiles/musiclab.dir/src/note/TimedNote.cpp.o.requires

.PHONY : CMakeFiles/musiclab.dir/requires

CMakeFiles/musiclab.dir/clean:
	$(CMAKE_COMMAND) -P CMakeFiles/musiclab.dir/cmake_clean.cmake
.PHONY : CMakeFiles/musiclab.dir/clean

CMakeFiles/musiclab.dir/depend:
	cd /home/alakt/musiclab/bin && $(CMAKE_COMMAND) -E cmake_depends "Unix Makefiles" /home/alakt/musiclab /home/alakt/musiclab /home/alakt/musiclab/bin /home/alakt/musiclab/bin /home/alakt/musiclab/bin/CMakeFiles/musiclab.dir/DependInfo.cmake --color=$(COLOR)
.PHONY : CMakeFiles/musiclab.dir/depend

