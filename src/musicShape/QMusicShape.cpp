#include "QMusicShape.hpp"  

void QMusicShape::setLayouts(){
    for(auto& note: notesInShape){
        note->setDrawingAttributes(m_basePosition + m_shiftX, 40, 1);
        m_shiftX += 100;
    }
}

void QMusicShape::paintEvent(QPaintEvent* event){ 
    QPainter painter(this);
    for(auto& note: notesInShape){
        note->paintNote(painter);

    }
}