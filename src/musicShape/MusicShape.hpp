#ifndef MUSICSHAPE_H
#define MUSICSHAPE_H
#pragma once

#include <vector>
#include <memory>
#include <iostream>

#include "../note/Note.hpp"


constexpr unsigned int MAX_NOTES_IN_SHAPE = 5;

/**
 * @brief A music shape is a set of timed notes. 
 * @class MusicShape
 */
class MusicShape
{
	public:

		template<typename ...Note>
		MusicShape(Note ...noteToAdd)
		{
			(addNoteToShape(noteToAdd), ...);

		}

		template<typename ...Note>
		void addNoteToShape(Note ...noteToAdd){
			(addNoteToShape(noteToAdd), ...);
		}   

		void addNoteToShape(std::shared_ptr<Note> noteToadd){
			notesInShape.push_back(noteToadd);
		}

		friend std::ostream& operator<<(std::ostream& os, const std::shared_ptr<MusicShape> musicShape);

		virtual ~MusicShape() {};
		
	protected:
		std::vector<std::shared_ptr<Note>> notesInShape;

};
#endif