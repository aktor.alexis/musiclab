#ifndef QMUSICSHAPE_H
#define QMUSICSHAPE_H
#pragma once

#include "MusicShape.hpp"
#include <QFrame>

constexpr int SPACE_BETWEEN_NOTES = 40;

class QMusicShape: public QFrame, public MusicShape
{
	public:
		template <typename ... note> 
		QMusicShape(int t_basePosition, note ... noteToAdd): m_basePosition(t_basePosition), MusicShape(noteToAdd...), QFrame(0){
		}

		void setLayouts();

		void paintEvent(QPaintEvent* event);

		virtual ~QMusicShape(){}
		
	private:
		int m_basePosition;
		int m_shiftX = 0;
};
#endif