#include "MusicShape.hpp"  

std::ostream& operator<<(std::ostream& os, const std::shared_ptr<MusicShape> musicShape){
    for(auto& note: musicShape->notesInShape){
        os << note << std::endl; 
    }
    return os;
}