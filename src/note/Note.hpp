#ifndef __TIMEDNOTE_H__
#define __TIMEDNOTE_H__
#include "NoteTempo.hpp"
#include "NoteName.hpp"
#include <memory>
#include <QPainter>
#include <QFrame>

constexpr int LINE_NOTE_SHIFT 		= 10;
constexpr int PEN_SIZE 				= 3;
constexpr int LINE_NOTE_SIZE 		= 35;
constexpr int CIRCLE_NOTE_WIDTH 	= 10;
constexpr int CIRCLE_NOTE_HEIGHT 	= 8;

class Note: public QFrame{
    public:
        Note(NoteName t_noteName, unsigned int t_scale, NoteTempo t_tempo);

        virtual ~Note(){}

		virtual void paintNote(QPainter& painter) = 0;

        void setDrawingAttributes(unsigned int posX, unsigned int posY, unsigned int scale);

        friend std::ostream& operator<<(std::ostream& os, const std::shared_ptr<Note> tNote);
        
    protected:
        NoteName        m_noteName;
        unsigned int    m_scale;
        NoteTempo       m_tempo;
		unsigned int 	m_posX;
		unsigned int 	m_posY;
		unsigned int 	m_graphicScale;
        bool            m_isDrawable;
};
#endif // __TIMEDNOTE_H__