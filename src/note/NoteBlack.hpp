#ifndef NOTEBLACK_H
#define NOTEBLACK_H
#pragma once
#include "Note.hpp"

class NoteBlack: public Note
{
	private:

	public:
		NoteBlack(NoteName t_noteName, unsigned int t_scale, NoteTempo t_tempo);

		virtual ~NoteBlack(){}

		void paintNote(QPainter& painter);

};
#endif