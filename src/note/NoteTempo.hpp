#ifndef __NOTETEMPO_H__
#define __NOTETEMPO_H__

#include <iostream>
/**
 * @enum NoteTempo
 * @brief Contains all possible tempo.
 */
enum class NoteTempo{
    ROUND  = 0,
    WHITE  = 1,
    BLACK  = 2,
    CROSH  = 3,
    DCROSH = 4,
    TCROSH = 5
};

/**
 * @brief Convert note tempo enum to a string.
 * @param noteName the note tempo to convert.
 * @return A string containing the note tempo. 
 */
inline std::string fromNoteTempoEnumToStr(NoteTempo noteName){
    switch(noteName){
        case NoteTempo::ROUND:
            return "ROUND";
            break;
        case NoteTempo::WHITE:
            return "WHITE";
            break;
        case NoteTempo::BLACK:
            return "BLACK";
            break;
        case NoteTempo::CROSH:
            return "CROSH";
            break;
        case NoteTempo::DCROSH:
            return "DCROSH";
            break;
        case NoteTempo::TCROSH:
            return "TCROSH";
            break;
        default:
            return "None";
            break;
    }
}

#endif // __NOTETEMPO_H__