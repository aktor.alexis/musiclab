#ifndef NOTEWHITE_H
#define NOTEWHITE_H
#pragma once

#include "Note.hpp"

class NoteWhite: public Note 
{
	private:

	public:
		NoteWhite(NoteName t_noteName, unsigned int t_scale, NoteTempo t_tempo);

		void paintNote(QPainter& painter);

		virtual ~NoteWhite(){}
};
#endif