#include "Note.hpp"

std::ostream& operator<<(std::ostream& os, const std::shared_ptr<Note> note){
    os << "NoteName: " << fromNoteEnumToStr(note->m_noteName) << ", scale: " << note->m_scale << ", tempo: " << fromNoteTempoEnumToStr(note->m_tempo);
    return os;
}

Note::Note(NoteName t_noteName, unsigned int t_scale, NoteTempo t_tempo): 
                                                m_noteName(t_noteName), 
                                                m_scale(t_scale), 
                                                m_tempo(t_tempo),
                                                m_posX(0),
                                                m_posY(0),
                                                m_graphicScale(0),
                                                m_isDrawable(false)
                                                {}

void Note::setDrawingAttributes(unsigned int posX, unsigned int posY, unsigned int scale){
    m_posX = posX;
    m_posY = posY;
    m_graphicScale = scale;
    m_isDrawable = true;
}