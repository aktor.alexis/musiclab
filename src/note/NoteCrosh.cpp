#include "NoteCrosh.hpp"  

NoteCrosh::NoteCrosh(NoteName t_noteName, unsigned int t_scale, NoteTempo t_tempo): Note(t_noteName, t_scale, t_tempo){

}

void NoteCrosh::paintNote(QPainter& painter){
    painter.setBrush(Qt::black);
    painter.setPen(QPen(Qt::black, PEN_SIZE));
    painter.drawLine(QLineF(QPointF(m_posX - LINE_NOTE_SHIFT, m_posY), QPointF(m_posX - LINE_NOTE_SHIFT, m_posY - LINE_NOTE_SIZE)));
    painter.drawEllipse(QPointF(m_posX, m_posY), CIRCLE_NOTE_WIDTH * m_graphicScale, CIRCLE_NOTE_HEIGHT * m_graphicScale);
    painter.drawLine(QLineF(QPointF(m_posX - LINE_NOTE_SHIFT -10, m_posY - LINE_NOTE_SIZE + 15), QPointF(m_posX - LINE_NOTE_SHIFT, m_posY - LINE_NOTE_SIZE)));
}
