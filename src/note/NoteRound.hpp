#ifndef NOTEROUND_H
#define NOTEROUND_H
#pragma once

#include "Note.hpp"

class NoteRound: public Note
{
	public:
		NoteRound(NoteName t_noteName, unsigned int t_scale, NoteTempo t_tempo);

		void paintNote(QPainter& painter);

		virtual ~NoteRound(){}

};
#endif