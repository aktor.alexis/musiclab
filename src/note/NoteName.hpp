#ifndef __NOTENAME_H__
#define __NOTENAME_H__
#include <iostream>

/**
 * @enum NoteName
 * @brief Enum all possible note names.
 */
enum class NoteName{
    C       = 0,
    C_SHARP = 1,
    D       = 2,
    D_SHARP = 3,
    E       = 4,
    E_SHARP = 5,
    F       = 6,
    F_SHARP = 7,
    G       = 8,
    G_SHARP = 9,
    A       = 10,
    A_SHARP = 11,
    B       = 12,
    B_SHARP = 13
};

/**
 * @brief Convert note enum to a string.
 * @param noteName the note to convert.
 * @return A string containing the note name. 
 */
inline std::string fromNoteEnumToStr(NoteName noteName){
    switch(noteName){
        case NoteName::C:
            return "C";
            break;
        case NoteName::C_SHARP:
            return "C_SHARP";
            break;
        case NoteName::D:
            return "D";
            break;
        case NoteName::D_SHARP:
            return "D_SHARP";
            break;
        case NoteName::E:
            return "E";
            break;
        case NoteName::E_SHARP:
            return "E_SHARP";
            break;
        case NoteName::F:
            return "F";
            break;
        case NoteName::F_SHARP:
            return "F_SHARP";
            break;
        case NoteName::G:
            return "G";
            break;
        case NoteName::G_SHARP:
            return "G_SHARP";
            break;
        case NoteName::A:
            return "A";
            break;
        case NoteName::A_SHARP:
            return "A_SHARP";
            break;
        case NoteName::B:
            return "B";
            break;
        case NoteName::B_SHARP:
            return "B_SHARP";
            break; 
        default:
            return "None";
            break;
    }
}

#endif // __NOTENAME_H__