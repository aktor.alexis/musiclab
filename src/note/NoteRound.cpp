#include "NoteRound.hpp"  

NoteRound::NoteRound(NoteName t_noteName, unsigned int t_scale, NoteTempo t_tempo): Note(t_noteName, t_scale, t_tempo){

}	

void NoteRound::paintNote(QPainter& painter){
    painter.setPen(QPen(Qt::black, PEN_SIZE));
    painter.setBrush(Qt::white);
    painter.drawEllipse(QPointF(m_posX,m_posY), CIRCLE_NOTE_WIDTH * m_graphicScale, CIRCLE_NOTE_HEIGHT * m_graphicScale);
}