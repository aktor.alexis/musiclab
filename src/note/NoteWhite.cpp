#include "NoteWhite.hpp"  

NoteWhite::NoteWhite(NoteName t_noteName, unsigned int t_scale, NoteTempo t_tempo): Note(t_noteName, t_scale, t_tempo){

}	

void NoteWhite::paintNote(QPainter& painter){
    painter.setPen(QPen(Qt::black, PEN_SIZE));
    painter.setBrush(Qt::white);
    painter.drawLine(QLineF(QPointF(m_posX - LINE_NOTE_SHIFT, m_posY), QPointF(m_posX - LINE_NOTE_SHIFT, m_posY - LINE_NOTE_SIZE)));
    painter.drawEllipse(QPointF(m_posX,m_posY), CIRCLE_NOTE_WIDTH * m_graphicScale, CIRCLE_NOTE_HEIGHT * m_graphicScale);
}
