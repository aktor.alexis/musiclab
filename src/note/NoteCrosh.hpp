#ifndef NOTECROSH_H
#define NOTECROSH_H
#pragma once
	
#include "Note.hpp"

class NoteCrosh: public Note 
{
	private:

	public:
		NoteCrosh(NoteName t_noteName, unsigned int t_scale, NoteTempo t_tempo);

		void paintNote(QPainter& painter);

		virtual ~NoteCrosh(){}

};
#endif