#include <iostream>
#include "note/Note.hpp"
#include "musicShape/MusicShape.hpp"
#include <QApplication>
#include <QPushButton>
#include <QPainter>
#include <QGridLayout>
#include "musicShape/MusicShape.hpp"
#include "musicShape/QMusicShape.hpp"
#include "note/NoteBlack.hpp"
#include "note/NoteCrosh.hpp"
#include "note/NoteRound.hpp"
#include "note/NoteWhite.hpp"

int main(int argc, char *argv[]) {

	

	QApplication a(argc, argv);
	
	std::shared_ptr<Note> note1 = std::make_shared<NoteBlack>(NoteName::A_SHARP, 0, NoteTempo::BLACK);
	std::shared_ptr<Note> note2 = std::make_shared<NoteWhite>(NoteName::A_SHARP, 0, NoteTempo::BLACK);
	std::shared_ptr<Note> note3 = std::make_shared<NoteRound>(NoteName::A_SHARP, 0, NoteTempo::BLACK);
	std::shared_ptr<Note> note4 = std::make_shared<NoteCrosh>(NoteName::A_SHARP, 0, NoteTempo::BLACK);

	std::shared_ptr<QMusicShape> shape1 = std::make_shared<QMusicShape>(100, note1, note2, note3, note4);

  	//QPushButton button ("Hello world !");
	shape1->setLayouts();
	shape1->show();

    return a.exec();
}
